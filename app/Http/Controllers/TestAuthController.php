<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestAuthController extends Controller
{
    private $client_website;
    private $client_id;
    private $client_secret;

    public function __construct()
    {
        $this->client_website = env('OAUTH2_WEBSITE');
        $this->client_id = env('OAUTH2_CLIENT_ID');
        $this->client_secret = env('OAUTH2_CLIENT_SECRET');
    }

    public function index()
    {
        $query = http_build_query([
            'client_id' => $this->client_id,
            'redirect_uri' => url('callback'),
            'response_type' => 'code',
            'scope' => ''
        ]);

        return redirect($this->client_website . '/oauth/authorize?' . $query);
    }

    public function callback(Request $request)
    {
        $response = (new \GuzzleHttp\Client)->post($this->client_website . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'client_id' => $this->client_id,
                'client_secret' => $this->client_secret,
                'redirect_uri' => url('callback'),
                'code' => $request->code,
            ]
        ]);

        session()->put('token', json_decode((string)$response->getBody(), true));

        return redirect('/rental-listing');
    }

    public function user()
    {
        $response = (new \GuzzleHttp\Client)->get($this->client_website . '/api/user', [
            'headers' => [
                'Authorization' => 'Bearer ' . session()->get('token.access_token')
            ]
        ]);

        return response()->json(json_decode((string)$response->getBody(), true));
    }

    public function rentalListing()
    {
        $response = (new \GuzzleHttp\Client)->get($this->client_website . '/api/rental-listing', [
            'headers' => [
                'Authorization' => 'Bearer ' . session()->get('token.access_token')
            ]
        ]);

        return response()->json(json_decode((string)$response->getBody(), true));
    }

    public function token() {
        return 'Bearer ' . session()->get('token.access_token');
    }

    public function rentalListingCoordinates()
    {
        $response = (new \GuzzleHttp\Client)->get($this->client_website . '/api/rental-listing-coordinates-by-community', [
            'headers' => [
                'Authorization' => 'Bearer ' . session()->get('token.access_token')
            ],
            'query' => [
                'community_id' => 1,
            ]
        ]);

        return response()->json(json_decode((string)$response->getBody(), true));
    }
}
