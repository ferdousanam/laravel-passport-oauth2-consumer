<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TestAuthController@index');
Route::get('/callback', 'TestAuthController@callback');
Route::get('/token', 'TestAuthController@token');
Route::get('/user', 'TestAuthController@user');
Route::get('/rental-listing', 'TestAuthController@rentalListing');
Route::get('/rental-listing-coordinates', 'TestAuthController@rentalListingCoordinates');
